<?php
require_once (dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "hm_13" . DIRECTORY_SEPARATOR . "config.php");

$directory = ROOT_PATH . DIRECTORY_SEPARATOR;

if (! file_exists($directory . "users.json")) {
    //Reading text file
    $fileString = file_get_contents($directory . "users.txt");
    //Replacing symbols \t\n\r\0\x0B to space 
    $replSymbols = ["\t", "\r", "\n", "\0", "\x0B"];
    $fileString = str_replace($replSymbols, " ", $fileString); 
    //Deleting repeated spaces
    $fileString = preg_replace('/^ +| +$|( ) +/m', '$1', $fileString);
    //echo $fileString . "<br>";
    //Exploding string into array
    $fileArray = explode(" ", $fileString);
    //Creating users array
    $arrayKeys = ["name", "login", "password", "email", "lang"];
    $sizeArrayKeys = count($arrayKeys);
    $i = 0;
    $j = 0;
    do {
        $users[$arrayKeys[$j++]][] = $fileArray[$i++];
        if ($j == $sizeArrayKeys) {
            $j = 0;
        }
        if (! isset($fileArray[$i])) {
            $fileArray[$i] = null;
        }
    }
    while ($fileArray[$i] == true);

    file_put_contents("users.json" , json_encode($users));
} 

//Function for opening specified file
function openJsonFile($address)
{
    $usersJson = file_get_contents($address);
    $users = json_decode($usersJson, true);
    return $users;
}

//Function for finding user from login and password
function findLoginPasswordUser($login, $password, $usersArray)
{
    $checkBool [0] = false;
    $checkBool [1] = false;
    $i = 0;

    foreach ($usersArray['login'] as $log) {
        if ($log == $login) {//Checking the login
            $checkBool [0] = true;
            if ($password == $usersArray['password'][$i]) {
                //Checking the password
                $checkBool [1] = true;
            }
            break;
        }
        $i++;
    }

    if ($checkBool [0] == true){
        //If user with specified login is exists
        if ($checkBool [1] == true) {
            //If user did correct enter his password
            $keys = array_keys($usersArray);
            foreach ($keys as $key) {
                $userData[$key][] = $usersArray[$key][$i];
            }
            return $userData;
        } else {
            return "Incorrect entering password<br>";
        }
    } else {
        return "User with this login isn't exists<br>";
    }
}

$users = openJsonFile($directory . "users.json");
echo "<pre>";
var_dump($users);
echo "</pre>";

if (!empty($_POST['login']) && !empty($_POST['password'])) {
    $userData = findLoginPasswordUser($_POST['login'], $_POST['password'], $users);
    echo "User Data: " . "<br>";
    echo "<pre>";
    var_dump($userData);
    echo "</pre>";
}
else {
    header("Location: /hm_13/index.php?error=1");
    die();
}